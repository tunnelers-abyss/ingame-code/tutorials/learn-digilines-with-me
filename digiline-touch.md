# Digiline touch screens

## Preface:

I am sivarajan, Moderator of Tunnelers Abyss Server, Advanced Trains, Digiline, Mesecons, Pipeworks and Technic Expert. I love messing around with things. I am here to present you the guide to use digiline touch screen from digistuff mod. This guide is desgined in a way that everyone who knows digiline can use this. If you do not know the digiline basic check out my Noobs guide to digiline here :https://h2mm.gitlab.io/web/tutorials/digilines.html . There is an example code at end of every lesson (Except for some lessons. contributions are welcome.) to get better understanding of the digiline hardware. Thank you for reading this. I hope you I did my best to make you understand this compoent. Good luck and Have fun.
-Yours Sincerly 
  Sivarajan


## Introduction: 
this is a component from digistuff that lets you make your own GUI controller for the lua controller.

## Lesson 1: Basics of touch screen.
Introduction: Touch screen can have various components like button, textview, text field, dropdown menu etc... before we begin learning these we need to learn program structure of how we can build a touch screen GUI and basic program structure.

Here is the recommended basic program structure of a touch screen:
```
workspace1 ={
   command_table1,  command_table2 ... command_tablen
}
...
...
workspaceN{
  command_table1,  command_table2 .. command_tablen
}

if event.type == "digiline" then
    --block that handles events of the component
    
elseif event.type == "program" then
    -- This block consist of initialization of touch screen
    -- mem variable initialization block
    digiline_send("<channel of touch screen>",workspace)
end
```

The Workspace Variable:

- Note: the workspace variable that you are going to use can be of any name.
- This variable stores the digiline table to be sent to the touch screen. If you store it in form of a variable it will be helpful to modify values of each component specified.
- commands that will add button and other components goes here.
- The workspace variable can contain many commands. Each command is enclosed in a table. In between each table there is comma.
- To modify the properties of component you can proceed as follows:

```
        workspace = {
        { --workspace[1]
            command ="clear"
        },
            {--workspace[2]
            label="A text will be shown here",
            command="addlabel",
            X=0.7, W=1.2, Y=3.9, H=0.8
            }
        }
    
```
Here we need to modify the label "a text will shown".
It is proceed like this.
 workspace[2].label = "Hello world".
(Here 2nd table of workspace has a key called label. )

now we need to know the contents of workspace variable. From the format that I have laid out, let us list out how command table looks like.  

The table looks like this:
```
{

    command= "<a valid command>"  
    X=0.8, W=7.6, Y=0.8, H=0.8, 
    
    -- other properties of compoents
}
```
Where
X,Y - Position of the component (Applies to placable components not for other. If value is greater than 10, you cannot see component.)
W,H - The width and height of the component
### The vaild commands are:

1.  realcoordinates         - Changes the formspec to use real cordinate style.
2.  addimage                - Adds an image.(Images of texture of a node is used here)
3.  addfield                - Adds a textbox/Text field where user can type single line input.
4.  addpwdfield             - Adds a password text field.
5.  addtextarea             - Adds a big text area with multi line input.
6.  addlabel                - Adds a text label.
7.  addvertlabel            - Adds a vertical label.    
8.  addbutton               - Adds a button.
9.  addimage_button         - Adds a button with image. (Images of texture of a node is used here).
10. addimage_button_exit    - Adds an image button with exit command.
11. addbutton_exit          - Adds an exit button.
12. addtextlist             - Adds a list of text.
13. adddropdown             - Adds a dropdown list.
14. lock                    - Restricts other player usage who are not owner of the area.
15. unlock                  - Removes other player restriction.
16. clear                   - Clears the display. (This should be run everytime when updating the display).

The properties of each compoents are expressed in the upcomming Lessons.


## Lesson 2:  Labels,and Button.
### 2.1) Labels:    
    
Labels are core compoent of a GUI application. It displays text. It does not respond to any click event.
To add a label one must have this table inside the workspace table. The general format of the table will be:


    {
        command= "addlabel",
        label="<Text to be displayed>",
        X=<0 to 10>, W=<0 to 10>, Y=<0 to 10>, H=<0 to 10>, -- The cordinates (x,y) and (w,h)
    
    }
   


Note: The width and height do not affect the font size but, it increases the amount of space that label componet occupies the screen.
example workspace variable:

```
    workspace = {
        {
            command ="clear"
        },
        {
            label="A text will be shown here",
            command="addlabel",
            X=0.7, W=1.2, Y=3.9, H=0.8
        }
    }
```

### 2.2) Buttons:
    
Button are A component of GUI that will trigger event.It responds to click events.
To add a button there must be this table inside the workspace table, the general format of the table will be: 
    
```
 {
    command= "addbutton" ,
    label = "<button text>",
    name="<button unique name identifier>",
    image="<valid texture>", -- if command is "addimage_button" or "addimage_button_exit", this field is valid
    X=<0 to 10>, W=<0 to 10>, Y=<0 to 10>, H=<0 to 10>, -- The cordinates (x,y) and (w,h)
}
    Example workspace variable with button b1:
    workspace = {
        {
            command="clear",
        },{
            command="addbutton",
            label="Click me",
            name="b1",
            X=0.8, W=7.6, Y=0.8, H=0.8,
            }
    }
    The event table we get when we press a particular button will look like:
    {
        type = "digiline",
        channel = "<channel name>",
        msg = {
                clicker = "<person who cliked the button>",
                <button_name>= "<button_label>"       
                }
    }   
    Example:
    event = {
        type = "digiline",
        channel = "touch",
        msg = {
                clicker = "sivarajan",
                b1 = "Click me"
            }
    }
```

### 2.3) How to implement a basic digiline touch screen code:
    
Aim: To make a digiline Application for a counter.

Aparatus Required: Digiline Touch Screen, Lua controller and digiline wire.
Procedure
1) Give a channel name to the touch screen (here it is Touch)
2) Now connect lua controller with touch screen.
3) Inser the code below.
4) click execute.
Code:

```
    workspace = {
        {
            command="clear", -- clears the touch screen
        },{
            command="addbutton", -- adds button with these properties
            label="Click me",
            name="b1",
            X=0.8, W=7.6, Y=0.8, H=0.8,
        },{
            command="addlabel", -- adds label with these properties
            label=tostring(mem.var),
            Y=1.9, W=1.2, H=0.8, X=2.3,
        },
        {
        command="unlock" -- unlocks the touch screen for everyone's usage
        }
    }

    if event.type =="digiline" then
        -- this event is triggered when the button is pressed
        mem.var = mem.var +1
        workspace[3].label = tostring(mem.var) -- we modify label value in 3rd table.
        digiline_send("touch",workspace) -- we send the modified workspace to touch screen
    elseif event.type == "program"  then
        mem.var=0 -- initialize mem.var = 0 when execute is presssed on lua controller
        digiline_send("touch",workspace) -- send the workspace variable content
    end
```

Also Note that when the player exits the form the following event table is sent to lua controller: 

```
{
    type = "digiline",
    channel = "touch",
    msg = {
        clicker = "sivarajan",
        quit = "true"
    }
}
```

#### Try it yourself: make a digiline touch interface with buttons to control your ports in lua controller.

## Lesson 3: Text Input Field.

Text Input Field enables to get user input.To add a TextField there must be this table inside the workspace table in this format. The general format of the table will be:

```
{
    command="addfield", -- "addpwdfield" adds password text field. "addtextarea" adds a large text field
    label="<label on the text field (hint)>",
    name="<name of the component>",
    default="<default text inside the component>",
    X=<0 to 10>, W=<0 to 10>, Y=<0 to 10>, H=<0 to 10>,
}
```

Example workspace variable:

```
workspace = {
    {
        command="clear",
    },{
        label="type something here",
        command="addfield",
        name="f1",
        default="text1",
        W=8, H=0.8, X=0.7, Y=1.9,
    }

}
```
Also text field cannot trigger an event. In this case a button is used to trigger the event. The event table look this if Button is triggered:

```
    event ={
        type = "digiline",
        channel = "touch",
        msg = {
            <filedname> = "<text inside the field>",
            <button name> = "<value>",
            clicker = "<player name>",
        }
    }
```
Example event table:

```
event = {
    type = "digiline",
    channel = "touch",
    msg = {
        field1 = "hello world",
        button1 = "send",
        clicker = "sivarajan",
    }
}
```
### Example Code: A program to do simple arithematic operation on 2 number
Try the following code in your Lua controller.

```
workspace = {
    {
        command="clear",
    },
    {
        label="number 2:",
        command="addfield",
        name="fnum2",
        default="",
        W=7.6, H=0.8, X=0.7, Y=3.1,
    },
    {
        label="number 1:",
        command="addfield",
        name="fnum1",
        default="",
        W=7.6, H=0.8, X=0.7, Y=1.9,
    },
    {
        command="addbutton",
        label="Divide",
        name="btndiv",
        X=4.4, W=1.6, Y=6.8, H=0.8,
    },
    {
        command="addbutton",
        label="Multiply",
        name="btnmul",
        Y=6.8, W=1.6, H=0.8, X=0.8,
    },
    {
        command="addbutton",
        label="subtract",
        name="btnsub",
        X=4.4, W=1.6, H=0.8, Y=5.6,
    },
    {
        label="Add",
        name="btnadd",
        command="addbutton",
        X=0.8, W=1.6, Y=5.6, H=0.8,
    },
    {
        command="addlabel",
        label="Simple Calculator",
        X=3.1, W=1.2, H=0.8, Y=0.7,
    },
    {
        label="Answer",
        command="addfield",
        name="f_answer",
        default="",
        W=1.6, H=0.8, X=0.7, Y=4.3,
    }
}
if event.type == "digiline" then
    --block that handles events of the component
    
    if event.msg.btnadd then
        c = tonumber(event.msg.fnum1)+tonumber(event.msg.fnum2)
        workspace[#workspace].default = tostring(c)
        workspace[2].default = event.msg.fnum2
        workspace[3].default = event.msg.fnum1
    
    elseif event.msg.btnsub then
        c = tonumber(event.msg.fnum1)-tonumber(event.msg.fnum2)
        workspace[#workspace].default = tostring(c)
        workspace[2].default = event.msg.fnum2
        workspace[3].default = event.msg.fnum1
    
    elseif event.msg.btnmul then
        c = tonumber(event.msg.fnum1)*tonumber(event.msg.fnum2)
        workspace[#workspace].default = tostring(c)
        workspace[2].default = event.msg.fnum2
        workspace[3].default = event.msg.fnum1
        
    elseif event.msg.btndiv then
        a = tonumber(event.msg.fnum1)
        b =tonumber(event.msg.fnum2)
        c =  a/b
        workspace[#workspace].default = tostring(c)
        workspace[2].default = event.msg.fnum2
        workspace[3].default = event.msg.fnum1
        
    end
    digiline_send("touch",workspace)
    digiline_send("ter",event)
elseif event.type == "program" then
    -- This block consist of initialization of touch screen
    -- mem variable initialization block
    digiline_send("touch",workspace)
end
```
## Lesson 4: TextList And dropdown list.
### 4.1) Textlist.
It is a component where we can give the user list of items.
It triggers an event on selecting an element.
This table is to be inserted in the workspace variable to create textlist:
```
    {
        command="addtextlist",
        selected_id=<integer less than amount of element>,
        label="<string>", -- text shown top of the list
        listelements={
        "item 1",
        "item 2",
            ....
            "item N"-- elements in a list are suceded by comma
        },      ,--
        name="<name of the list>",
        transparent=<boolean>,-- if true the list willl be transparent
        X=<0 to 10>, W=<0 to 10>, Y=<0 to 10>, H=<0 to 10>
    }
```

Example workspace variable:
```
    workspace = {
        {
            command="clear",
        },
        {
            selected_id=1,
            label="mytextlist",
            listelements={
            "item2",
            "item1",
            },      command="addtextlist",
            name="textlist1",
            transparent=false,
            W=2.6, H=1.8, X=0.8, Y=0.8,
        }
    
    }

```

When you select a component from the list, Then the event table looks like this:

```
event = {
    type = "digiline",
    channel = "touch",
    msg = {
        textlist1 = "CHG:1", -- CHG:(index of element that the player selected)
        clicker = "Antares", -- player name who slected it
    }
}
```
## 4.2) dropdown menu

It is a component where we can choice of items that can be selected by user. It is same is text list but difference is it occupies less space. When the user selects the arrow the list of choices are shown. 
It triggers an event on selecting an element.

To add dropdown menu there must be this table inside the workspace table in this format. The general format of the table will be:

```
{   
    command="adddropdown",
    selected_id=<the element that is selected (integer value)>,
    choices={
    -- list of choices seperated by comma
    "item1",
    ....
    "item n"
    },      
    label="mydropdownlist",
    name="dropdown1",
    X=<0 to 10>, W=<0 to 10>, Y=<0 to 10>, H=<0 to 10>
}
```

Example Workspace variable:
```
{
    command="adddropdown",
    label="mydropdownlist",
    name="dropdown1",
    W=1.6, H=0.8, X=0.8, Y=4.4,
    selected_id=1,
    choices={
        "item1",    w
        "item2",
    },    
    
}
```
This is how the event table looks like when a choice is selected:
```
event = {
    type = "digiline",
    channel = "touch",
    msg = {
        clicker = "sivarajan",
        dropdown1 = "item1" --here what ever item that is selected by user is shown.
    }
}
```

## Example code 
### A program to show list of items that can be bought with various currency (Credits Antares).

```
local function split(str, delimiter)
-- an useful function to split a string with delimiter. Thanks to flux for the code.
    local rv = {}
    local i, j = str:find(delimiter, nil, true)
    while i do
        table.insert(rv, str:sub(1, i - 1))
        str = str:sub(j + 1)
        i, j = str:find(delimiter, nil, true)
    end
    table.insert(rv, str)
    return rv
end


workspace ={
  {
    command="clear",
  },{
    selected_id=1,
    label="Item to buy",
    listelements={
      "Glider",
      "Car",
      "Boat",
      "Bike",
    },
     command="addtextlist",
      name="item",
      transparent=false,
      W=2.6, H=1.8, X=0.8, Y=0.8,
  },{
    selected_id=1,
    choices={
      "Dollars",
      "Gold Ingots",
      "Iron Ingots",
      "Diamonds",
    },      command="adddropdown",
      label="Payment by",
      name="currency",
      W=1.6, H=0.8, Y=4.4, X=0.8,
  },{
    label="VEHICLE SHOP",
    command="addlabel",
    X=3.9, W=1.2, Y=0.3, H=0.8,
  },{
    label="Transaction: ",
    command="addlabel",
    X=4.0, W=1.2, Y=3.9, H=0.8,
  }
}

if event.type =="digiline" then
    if event.msg.currency then
      mem.pay=event.msg.currency
      digiline_send("ter","Currency changed")
    end
    if event.msg.item then
      mem.buy=tostring(event.msg.item)
      digiline_send("ter","variable:"..mem.buy)
      n = tonumber(split(mem.buy, ":")[2]) -- we are splitting the CHG:2 here to get the index of the element
      mem.buy= workspace[2].listelements[n] -- now we get the selected element from the workspace

      digiline_send("ter","Item changed")
    end


    workspace[5].label="Ordering a"..mem.buy.." and pay it in "..mem.pay.."?"

    digiline_send("ter",event)
    digiline_send("touch",workspace)

elseif event.type =="program" then
    digiline_send("touch",workspace)
    mem.pay="[choose]"
    mem.buy="[choose]"
end





```







## Lesson 5) Images.

Image Component helps you to  place image in the touch screen.
It does not trigger an event.
To add image there must be this table inside the workspace table in this format. The general format of the table will be:

```
{
    command="addimage",
    texture_name= "<texture_name>",-- it is usually in the form of modname_nodename
    X=<0 to 10>, W=<0 to 10>, Y=<0 to 10>, H=<0 to 10>
}
```

Example workspace variable:

```
workspace ={
{
    command ="clear"
},
{
    command="addimage",
    texture_name= "default_stone.png",
    X=0,Y=0,W=5,H=5,

}
```

