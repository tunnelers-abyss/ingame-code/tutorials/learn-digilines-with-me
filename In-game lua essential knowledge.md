# Introudction 
This book has been written soley for purpose of making a in-game concepts more clear. This may be a right choice of document if you want to just learn parts of lua for digiline programming,

##  Display output to the console
To display a message to a console we can use print statement. The syntax is
```
print("hello world")

-- Charaters inside double quotes are considered as string. What ever you write it will be printed.
-- you can also print escape sequece like \n (new line ), \t(tab) just like C
```
## Trying out lua code on your machine.
To  try out lua code on Windows, Mac, or Linux here is the procedure:
1. Install lua compiler for your PC.
2. Open your favorite code editor (Vim, Emacs, Gedit, VS code, notepad etc...)
3. Type thhe lua code.
4. Save the file with .lua extention. example my_program.lua
5. Open your terminal.
6. type "lua path/to/your_file"  example: lua /home/sivarajan/my_program.lua
7. If the written code is correct output will be shown. 
8. If not Error messages will be displayed.

## constants.
Constants are the vaule that remain the same.
There are types of constant in lua:
- "hello world" : this is a string constant
- 10 : number

# Lua variables.
A variable is nothing but a name given to a storage area that our programs can manipulate. It can hold different types of values including functions and tables.

In Lua, though we don't have variable data types, we have three types based on the scope of the variable.

- Global variables − All variables are considered global unless explicitly declared as a local.

- Local variables − When the type is specified as local for a variable then its scope is limited with the functions inside their scope.

- Table fields − This is a special type of variable that can hold anything except nil including functions.

# Variable Definition in Lua
To define a varaiable We will use the following syntax:
```
type variable_name = value
```
Where:
type - either local for local variable leave it as such for tables and global variable.
variable_name : Valid variable name that does not contain any keywords.
example:
```
local a = 10 -- local variable with a number constant
b = "11"  -- global variable with string constant
c = {1,2} --table
```

# Conversion of data type
Lua is a dynamically typed language, so the variables don't have types, only the values have types. Values can be stored in variables, passed as parameters and returned as results.

In Lua, though we don't have variable data types, but we have types for the values. The list of data types for values are given below.
Sr.No 	Value Type & Description
1. nil- Used to differentiate the value from having some data or no(nil) data.
2. boolean - Includes true and false as values. Generally used for condition checking.
3. number- Represents real(double precision floating point) numbers.
4. string - Represents array of characters enclosed within double quotes
5. function -Represents a method that is written in C or Lua.
6. table - Represent ordinary arrays, symbol tables, sets, records, graphs, trees, etc., and implements associative arrays. It can hold any value (except nil).


We can use type function to display the type of constant that the variable holds.
- To convert a number to string here is how it is proceeded.
```
a = 10
b = tostring(a)
print(type(b))

```
- To convert a string to number
```
a ="10"
b = tonumber(a)
print(type(b))
```

##  print values of varaiable
To print values of variable we can just put the variable name inside the print()function like this.
Example
```
a=10
print(a)
```
if you  like to print some text before or after the variable availe uyou can concadiate with '..' (double dot).
Example:
```
b = 10
print("the value of b is "..b.."which is the smallest 2 digit number")

```
Note that a table cannot be printed with using print() we use loops to print contents on a table.

# Operations on variables.
## A) Arithematic Operation.
you can do all 4 basic arithemnatic operations on variable as follows: 
Exaple:
```
a = 10
b = 5 -- you can also do arithimatic operation between constants, constants and variable as well.
sum = a + b -- here we add variable a and b and assign it as value of C
product = a * b 
quotient = a / b
subraction = a - b
print("Sum: "..sum)
print("product: "..product)
print("quotient: "..quotient)
print("subtraction: "..subtraction)
```
## B) Relationsal Operation.
These are operators that helps with comparison of two numbers or string.
Following table shows all the relational operators supported by Lua language. Assume variable **A** holds 40 and variable **B** holds 50 then −

| Operator | Description | Example |
| --- | --- | --- |
| == | Checks if the value of two operands are equal or not, if yes then condition becomes true. | (A == B) is not true. |
| ~= | Checks if the value of two operands are equal or not, if values are not equal then condition becomes true. | (A ~= B) is true. |
| `>` | Checks if the value of left operand is greater than the value of right operand, if yes then condition becomes true. | (A  `>`  B) is not true. |
|  `<`  | Checks if the value of left operand is less than the value of right operand, if yes then condition becomes true. | (A `<`  B) is true. |
| `>=` | Checks if the value of left operand is greater than or equal to the value of right operand, if yes then condition becomes true. | (A `>=` B) is not true. |
| `<=` | Checks if the value of left operand is less than or equal to the value of right operand, if yes then condition becomes true. | (A  `<=`  B) is true. |
```

a = 21
b = 10

if( a == b )
then
   print("a is equal to b" )
else
   print("a is not equal to b" )
end

if( a ~= b )
then
   print("a is not equal to b" )
else
   print("2 - a is equal to b" )
end

if ( a < b )
then
   print("a is less than b" )
else
   print("a is not less than b" )
end

if ( a > b ) 
then
   print("a is greater than b" )
else
   print("a is not greater than b" )
end

-- Lets change value of a and b
a = 5
b = 20

if ( a <= b ) 
then
   print("a is either less than or equal to  b" )
end

if ( b >= a ) 
then
   print(" b is either greater than  or equal to b" )
end
```
C) Other Operators:



| Operator | Description | Example |
| --- | --- | --- |
| .. | Concatenates two strings. | a..b where a is &quot;Hello &quot; and b is &quot;World&quot;, will return &quot;Hello World&quot;. |
| # | An unary operator that return the length of the a string or a table. | #&quot;Hello&quot; will return 5 |
Example code:
```
str1 = "Hello "
str2 = "World"

print("Concatenation of string str1 with str2 is ", str1..str2 )
print("Length of str1 = ",#str1)

print("Length of str1 = ",#str2)
```
# Lua tables
Tables are the only data structure available in Lua that helps us create different types like arrays and dictionaries. Lua uses associative arrays and which can be indexed with not only numbers but also with strings except nil. Tables have no fixed size and can grow based on our need.
```
-sample table initialization
fruits= {}

--simple table value assignment
mytable[1]= "apple"
mytable[2] = "orange"
--removing reference
mytable = nil

-- lua garbage collection will take care of releasing memory
```
